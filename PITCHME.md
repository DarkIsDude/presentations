## Introduction to GitLab and GitLab CI

---

## In this talk

- Brief intro to Git and GitLab
- GitLab CI overview
- How GitLab CI works
- Resources to get started
